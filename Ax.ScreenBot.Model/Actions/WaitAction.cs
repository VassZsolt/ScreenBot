namespace Ax.ScreenBot.Model.Actions
{
    public class WaitAction : Action
    {
        public WaitAction() { }

        public WaitAction(uint waitTimeMs)
            : base(0)
        {
            WaitTimeAfterMs = waitTimeMs;
        }

        public override string ToString() => $"Wait for {WaitTimeAfterMs} ms.";
    }
}