using System.Drawing;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Serialization;
using WindowsInput.Events;

namespace Ax.ScreenBot.Model.Actions
{
    public class ClickAction : Action
    {
        [XmlElement(nameof(Point))]
        public Point Point { get; set; }

        public ClickAction() { }

        public ClickAction(Point point, uint waitTimeAfterMs)
            : base(waitTimeAfterMs)
        {
            Point = point;
        }

        public override async Task PerformAsync(CancellationToken cancellationToken)
        {
            await WindowsInput.Simulate.Events().MoveTo(Point.X, Point.Y).Click(ButtonCode.Left).Invoke();
            await base.PerformAsync(cancellationToken);
        }

        public override string ToString() => $"Click to: ({Point.X};{Point.Y}), and wait {WaitTimeAfterMs} ms.";
    }
}