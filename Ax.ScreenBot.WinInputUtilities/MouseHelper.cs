﻿using System;
using System.Runtime.InteropServices;

namespace Ax.ScreenBot.WinInputUtilities
{
    public static class MouseHelper
    {
        // https://docs.microsoft.com/en-us/windows/win32/api/winuser/nf-winuser-getcursorpos
        [DllImport("user32.dll", EntryPoint = "GetCursorPos")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool GetCursorPos_DLL(out IntegerPoint lpMousePoint);

        /// <summary> Returns the cursor's current position. </summary>
        public static (int x, int y) GetCursorPosition()
        {
            if (!GetCursorPos_DLL(out IntegerPoint point))
                throw new InvalidOperationException("Could not retrieve the mouse cursor's current position.");

            return (point.X, point.Y);
        }
    }
}