﻿using System.Drawing;
using System.Drawing.Imaging;

namespace Ax.ScreenBot.Gui.IoHandling.Screen
{
    public static class ScreenshotTaker
    {
        public static void SaveBitmap(Bitmap bmp, string path, ImageFormat imageFormat = null)
        {
            if (imageFormat == null)
                imageFormat = ImageFormat.Png;

            bmp.Save(path, imageFormat);
        }

        public static Bitmap TakeScreenshot(int fromX, int fromY, int toX, int toY)
        {
            Rectangle rect = new Rectangle(fromX, fromY, toX - fromX, toY - fromY);
            Bitmap bmp = new Bitmap(rect.Width, rect.Height, PixelFormat.Format32bppArgb);

            Graphics gfxScreenshot = Graphics.FromImage(bmp);
            gfxScreenshot.CopyFromScreen(rect.Left, rect.Top, 0, 0, bmp.Size, CopyPixelOperation.SourceCopy);

            return bmp;
        }

        public static Bitmap TakeScreenshot()
        {
            return TakeScreenshot(0, 0, System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width, System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height);
        }

        public static void SaveScreenshot(int fromX, int fromY, int toX, int toY, string path, ImageFormat imageFormat = null)
        {
            var bitmap = TakeScreenshot(fromX, fromY, toX, toY);
            SaveBitmap(bitmap, path, imageFormat);
        }

        public static void SaveScreenshot(string path, ImageFormat imageFormat = null)
        {
            SaveScreenshot(0, 0, System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width, System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height, path, imageFormat);
        }
    }
}
