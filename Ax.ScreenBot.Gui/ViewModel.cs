﻿using Ax.ScreenBot.Gui.Commands;
using Ax.ScreenBot.Gui.Extensions;
using Ax.ScreenBot.Gui.IoHandling.File;
using Ax.ScreenBot.Gui.IoHandling.Mouse;
using Ax.ScreenBot.Model.Actions;
using Ax.ScreenBot.MouseKeyHookIntegration;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Microsoft.Toolkit.Mvvm.ComponentModel;
using Microsoft.Toolkit.Mvvm.Input;
using Action = Ax.ScreenBot.Model.Actions.Action;

namespace Ax.ScreenBot.Gui
{
    internal sealed class ViewModel : ObservableObject
    {
        #region [ Command definitions ]

        public ICommand AddPositionsCommand { get; }
        public ICommand AddDelayCommand { get; }
        public ICommand StartReplayCommand { get; }
        public ICommand StopReplayCommand { get; }
        public ICommand DeleteAllStoredActionsCommand { get; }
        public ICommand Menu_HelpClickCommand { get; } = new RelayCommand(() => MessageBox.Show(Resources.Resource1.HelpText, "Help", MessageBoxButton.OK, MessageBoxImage.Information));
        public ICommand Menu_File_LoadClickCommand { get; }
        public ICommand Menu_File_SaveClickCommand { get; }

        #endregion

        #region [ Bound properties ]

        public CursorPositionProvider CursorPositionProvider { get; set; } = new CursorPositionProvider();
        public ObservableCollection<Action> StoredActions { get; set; } = new ObservableCollection<Action>();
        public GlobalMouseKeyHooker KeyAndMouseHooker { get; set; } = new GlobalMouseKeyHooker();
        public ReplayEngine ReplayEngine { get; }

        public uint ReplayCount { get; set; } = 1;
        public uint DelaySeconds { get; set; } = 0;
        public uint DelayMilliseconds { get; set; } = 0;

        public Visibility MainWindowVisibility
        {
            get => _mainWindowVisibility;
            set
            {
                _mainWindowVisibility = value;
                OnPropertyChanged();
            }
        }

        public bool IsReplayStartButtonEnabled
        {
            get => _isReplayStartButtonEnabled;
            set
            {
                _isReplayStartButtonEnabled = value;
                OnPropertyChanged();
                OnPropertyChanged(nameof(IsReplayStopButtonEnabled));
            }
        }
        
        public string ProgressNotificationText
        {
            get => _progressNotificationText;
            private set
            {
                _progressNotificationText = value;
                OnPropertyChanged();
            }
        }

        #endregion

        #region [ Calculated properties ]

        public bool IsReplayStopButtonEnabled => !IsReplayStartButtonEnabled;

        private uint TotalDelayMilliseconds => DelaySeconds * 1000 + DelayMilliseconds;
        
        #endregion

        public ViewModel()
        {
            AddPositionsCommand = new RelayCommand(AddPositions);
            AddDelayCommand = new RelayCommand(AddDelay);
            StartReplayCommand = new AsyncRelayCommand(StartReplay);
            StopReplayCommand = new RelayCommand(StopReplay);
            DeleteAllStoredActionsCommand = new RelayCommand(() => StoredActions.Clear());
            Menu_File_LoadClickCommand = new RelayCommand(() => new LoadFileCommandHandler(StoredActions).Execute());
            Menu_File_SaveClickCommand = new RelayCommand(() => new SaveFileCommandHandler(StoredActions).Execute());
            
            ReplayEngine = new ReplayEngine(str => ProgressNotificationText = str);

            KeyAndMouseHooker.KeyPressed += KeyHookerOnKeyPressed;
            KeyAndMouseHooker.ShiftMouseClicked += KeyHookerOnShiftMouseClick;
        }

        private void StopReplay()
        {
            ReplayEngine.Stop();
        }

        private void AddDelay()
        {
            StoredActions.Add(new WaitAction(TotalDelayMilliseconds));
        }

        private async Task StartReplay()
        {
            IsReplayStartButtonEnabled = false;
            await new ReplayCommandHandler().StartReplayAsync(ReplayEngine, StoredActions, ReplayCount);
            IsReplayStartButtonEnabled = true;
        }

        public void RemoveActions(IEnumerable<Action> actions)
        {
            foreach (var action in actions)
                StoredActions.Remove(action);
        }

        private void AddPositions()
        {
            MainWindowVisibility = Visibility.Collapsed;
            KeyAndMouseHooker.StartWatching();
        }

        private void KeyHookerOnShiftMouseClick(object sender, EventArgs args)
        {
            (int x, int y) currentMousePos = WinInputUtilities.MouseHelper.GetCursorPosition();
            StoredActions.Add(new ClickAction(currentMousePos.ToDrawingPoint(), TotalDelayMilliseconds));
        }

        private void KeyHookerOnKeyPressed(object sender, System.Windows.Forms.Keys key)
        {
            switch (key)
            {
                case System.Windows.Forms.Keys.CapsLock:
                    (int x, int y) currentMousePos = WinInputUtilities.MouseHelper.GetCursorPosition();
                    StoredActions.Add(new JumpAction(currentMousePos.ToDrawingPoint(), TotalDelayMilliseconds));
                    break;
                case System.Windows.Forms.Keys.Pause:
                    KeyAndMouseHooker.StopWatching();
                    MainWindowVisibility = Visibility.Visible;
                    break;
            }
        }

        #region [ Backing fields ]

        private Visibility _mainWindowVisibility = Visibility.Visible;
        private bool _isReplayStartButtonEnabled = true;
        private string _progressNotificationText;

        #endregion
    }
}